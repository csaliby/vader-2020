import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import Error from '../components/Error';

const useStyles = makeStyles(theme => ({
    heading: {
        fontFamily: 'Starjhol',
        fontSize: '200%',
        color: 'white',
        height: '100%'
    },
    message: {
        color: 'white'
    },
    img: {
        maxWidth: '70vw',
        maxHeight:'60vh'
    },
    button: {
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        marginBottom: theme.spacing(2)
    },
    buttonText: {
        fontFamily: 'Alatsi',
        fontSize: '125%',
        margin: '0'
    },
}));

export default function ThankYou({history, theme, tier, loggedIn}) {
    const classes = useStyles();

    if(loggedIn){
        switch(tier){
            case 0: {
                return (
                    <div style = {{backgroundColor: 'black'}}>
                        <h1 className = {classes.heading}>
                            thank you
                        </h1>
                        <img src = 'https://media3.giphy.com/media/SzQ5ao5QZ13mo/giphy.gif' alt = 'dancing' className ={classes.img}/>
                        <h3 className = {classes.message}>
                            Your donation has been processed successfully. Click the button below to return home:
                        </h3>
                        <Link to = '/'>
                            <Button 
                                color = 'primary' 
                                variant = 'contained'
                                className = {classes.button}>
                                    <p className = {classes.buttonText}><b>Return</b></p>
                                </Button>
                        </Link>
                    </div>
                );
            }
            case 1: {
                return (
                    <div style = {{backgroundColor: 'black'}}>
                        <h1 className = {classes.heading}>
                            thank you
                        </h1>
                        <img src = 'https://media.giphy.com/media/FpzXtbsW6VMPe/giphy.gif' alt = 'hallway' className = {classes.img}/>
                        <h3 className = {classes.message}>
                            Your donation has been processed successfully. Thank you for helping me stop the rebel scum.
                            Click the button below to return home:
                        </h3>
                        <Link to = '/'>
                            <Button 
                                color = 'primary' 
                                variant = 'contained'
                                className = {classes.button}>
                                    <p className = {classes.buttonText}><b>Return</b></p>
                                </Button>
                        </Link>
                    </div>
                );
            }
            case 2: {
                return (
                    <div style = {{backgroundColor: 'black'}}>
                        <h1 className = {classes.heading}>
                            thank you
                        </h1>
                        <img src = 'https://thumbs.gfycat.com/CarelessMixedEelelephant-size_restricted.gif' alt = 'hallway' className = {classes.img}/>
                        <h3 className = {classes.message}>
                            Your donation has been processed successfully, and it will fill our enemies with terror.
                            Click the button below to return home:
                        </h3>
                        <Link to = '/'>
                            <Button 
                                color = 'primary' 
                                variant = 'contained'
                                className = {classes.button}>
                                    <p className = {classes.buttonText}><b>Return</b></p>
                                </Button>
                        </Link>
                    </div>
                );
            }
        }
    }else{
        return(
            <Error message = 'You must be signed in to access this page.' />
        );
    }
};