import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Axios from 'axios';

const useStyles = makeStyles(theme => ({
  text: {
    color: 'white'
  },
  placeholder: {
    color: '#B3B3B3'
  },
  background: {
    display: 'flex',
    justifyContent: 'center',
    backgroundImage: "url('./assets/login/mustafar.jpg')",
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    minHeight: '94vh',
    width: '100%',
    left: '0',
    overflowX: 'hidden',
    position: 'absolute',
  },
  card: {
    marginTop: '1rem',
    backgroundColor: '#222222',
    opacity: '0.9',
    width: '100%',
    justifyContent: 'center',
    display: 'flex',
    flexDirection: 'column',
  },
  textField: {
    //width: '100%',
    margin: theme.spacing(2),
    display: 'flex'
  },
  button: {
    //width: '90%',
    marginRight: theme.spacing(2),
    marginLeft: theme.spacing(2),
    marginBottom: theme.spacing(2),
    display: 'flex'
  }
}));

export default function Login({login, theme, history}) {

    const classes = useStyles();

    const [inputFields, setInputFields] = useState({
        email: '',
        name: '',
        phoneNumber: '',
        password: '',
        password2: ''
      });

      const handleUserEmailChange = event => {
        setInputFields({ ...inputFields, email: event.target.value });
      };
      const handleNameChange = event => {
        setInputFields({...inputFields, name: event.target.value });
      };
      const handlePhoneNumberChange = event => {
        setInputFields({ ...inputFields, phoneNumber: event.target.value });
      }
      const handlePasswordChange = event => {
        setInputFields({ ...inputFields, password: event.target.value });
      };
      const handlepassword2Change = event => {
        setInputFields({...inputFields, password2: event.target.value});
      };

      const hasEmptyField = () => {
        return (inputFields.name === '' || inputFields.email === '' || inputFields.name === ''
                || inputFields.password === '' || inputFields.password2 === '');
      };
      //----------------------------------------------------------------------
      const handleLogin = async () => {
        //console.log('got here');
        const user = {
          email: inputFields.email,
          password: inputFields.password
        };
        try {
          await Axios.post(`/api/users/auth`, user).then(response => {
            const user = response.data;
            //console.log(user);
            if(user){
              login(user);
              history.push('/');
            }
            else {
              alert('Incorrect email or password.');
            }
          });
        }
        catch (error){
          console.log(error.message);
        }
      };
      //----------------------------------------------------------------------
      const handleRegistration = async () => {
        if (inputFields.password === inputFields.password2 && !hasEmptyField()) {
          const user = {
            email: inputFields.email,
            name: inputFields.name,
            phoneNumber: inputFields.phoneNumber,
            password: inputFields.password
          };
          try {
            await Axios.post('/api/users', user).then(response => {
              const user = response.data;
              //console.log(user);
              if(!user.message){
                handleLogin();
                history.push('/');
              }
              else{
                alert('An account with this email already exists.');
              }
            });
          }
          catch(error) {
            console.log(error.message);
          }
        }
        else if(hasEmptyField()){
          //console.log('empty field');
          alert('You have left a required field blank. Please fill out all required fields');
        }
        else if(inputFields.password !== inputFields.password2){
          //console.log('passwords dont match');
          alert('Passwords do not match.');
        }
      };
  
      return (
        <div className = {classes.background}>
          <div>
            <h3 style = {{fontFamily: 'Starjedi'}}>Log in to your account</h3>
            <Card className = {classes.card}>
              <div style = {{display: 'flex', flexDirection: 'row'}}>
                <TextField 
                  className = {classes.textField}
                  id = 'email-input'
                  label = 'Email'
                  InputProps = {{ className: classes.text}}
                  InputLabelProps = {{ className: classes.placeholder }}
                  margin = 'normal'
                  variant = 'filled'
                  onChange = {handleUserEmailChange}
                  required
                />
                <TextField
                  className = {classes.textField}
                  id = 'password-input'
                  label = 'Password'
                  type = 'password'
                  InputProps = {{ className: classes.text }}
                  InputLabelProps = {{ className: classes.placeholder }}
                  margin = 'normal'
                  variant = 'filled'
                  onChange = {handlePasswordChange}
                  required
                  style = {{float: 'right'}}
                />
              </div>
              <br />
              <Button 
                color = 'secondary' 
                variant = 'contained' 
                className = {classes.button}
                onClick = {handleLogin}>
                Log In
              </Button>
            </Card>
            <h3 className = {classes.text} style = {{fontFamily: 'Starjedi', marginTop: '2rem'}}>
              join me...
            </h3>
            <h3 style = {{fontFamily: 'Starjedi', marginTop: '-3vh'}}>
              together we can rule the galaxy
            </h3>
            <Card className = {classes.card}>
              <div style = {{display: 'flex', flexDirection: 'column'}}>
                <TextField 
                  className = {classes.textField}
                  id = 'email-input'
                  label = 'Email'
                  InputProps = {{ className: classes.text}}
                  InputLabelProps = {{ className: classes.placeholder }}
                  margin = 'normal'
                  variant = 'filled'
                  onChange = {handleUserEmailChange}
                  required
                />
                <div style = {{display: 'flex', flexDirection: 'row'}}>
                <TextField
                  className = {classes.textField}
                  id = 'name-input'
                  label = 'Name'
                  InputProps = {{ className: classes.text}}
                  InputLabelProps = {{ className: classes.placeholder }}
                  margin = 'normal'
                  variant = 'filled'
                  onChange = {handleNameChange}
                  required
                />
                <TextField
                  className = {classes.textField}
                  id = 'phone-number-input'
                  label = 'Phone number'
                  InputProps = {{ className: classes.text}}
                  InputLabelProps = {{ className: classes.placeholder }}
                  margin = 'normal'
                  variant = 'filled'
                  onChange = {handlePhoneNumberChange}
                />
                </div>
              </div>
              <br />
              <div style = {{display: 'flex', flexDirection: 'row'}}>
                <TextField
                  className = {classes.textField}
                  id = 'password-input'
                  label = 'Password'
                  type = 'password'
                  InputProps = {{ className: classes.text}}
                  InputLabelProps = {{ className: classes.placeholder }}
                  margin = 'normal'
                  variant = 'filled'
                  onChange = {handlePasswordChange}
                  required
                />
                <TextField
                  className = {classes.textField}
                  id = 'password-input'
                  label = 'Confirm Password'
                  type = 'password'
                  InputProps = {{ className: classes.text}}
                  InputLabelProps = {{ className: classes.placeholder }}
                  margin = 'normal'
                  variant = 'filled'
                  onChange = {handlepassword2Change}
                  required
                />
              </div>
              <br />
              <Button 
                color = 'secondary' 
                variant = 'contained' 
                className = {classes.button}
                onClick = {handleRegistration}>
                Register
              </Button>
            </Card>
          </div>
        </div>
      );
}