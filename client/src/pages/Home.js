import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import { Carousel } from 'react-responsive-carousel';
import { Link } from 'react-router-dom';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import VideoPanel from '../components/VideoPanel';

const useStyles = makeStyles(theme => ({
    mainCarousel: {
        height: '100%',
        width: '100%'
    },
    carouselImage: {
        maxHeight: '80vh',
    },
    textContainer: {
        width: '70%',
        paddingBottom: '1vh',
        backgroundColor: '#111111',
    },
    mainContainer: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'row',
        marginBottom: '5vh',
        backgroundColor: '#222222'
    },
    secondaryContainer: {
        width: '75%',
        height: '100%',
        display: 'flex',
        flexDirection: 'row',
        marginBottom: '5vh',
        backgroundColor: '#222222'
    },
    content: {
        height: '50%',
        width: '50%',
        color: 'white'
    },
    contentLarge: {
        height: '100%',
        width: '75%',
        backgroundColor: '#222222',
        color: 'white'
    },
    contentSmall: {
        height: '100%',
        width: '50%'
    },
    paragraph: {
        lineHeight: '1.5rem',
        textAlign: 'left',
        color: 'gray'
    },
    buttonText: {
        fontFamily: 'Alatsi',
        fontSize: '125%',
        margin: '0'
    },
    heading: {
        color: 'white',
        textAlign: 'left'
    },
    button: {
        width: '30%',
        padding: '0'
    },
}));

export default function Home({login, history, theme, loggedIn,resetPosition}) {
    resetPosition();
    const classes = useStyles();

    return (
        <div style = {{justifyContent: 'center'}}>
            <Container>
                <h1 style = {{fontFamily: 'Starjhol', fontSize: '350%'}}>darth vader for emperor 2020</h1>
                <h3 style = {{fontFamily: 'Starjedi', fontSize: '200%', marginTop: '-4vh'}}>it is useless to resist</h3>
            </Container>
            <hr />

            <Carousel
                className = {classes.mainCarousel} 
                showArrows = {true} 
                showStatus = {false}
                infiniteLoop = {true}
                autoPlay = {true}
                //dynamicHeight = {true}
                stopOnHover = {false}>
                <div>
                    <img src = './assets/home/vader2.jpg' className = {classes.carouselImage}/>
                </div>
                <div>
                    <img src = './assets/home/vader1.jpg'className = {classes.carouselImage}/>
                </div>
                <div>
                    <img src = './assets/home/vader-vs-obiwan.jpg' className = {classes.carouselImage}/>
                </div>
                <div>
                    <img src = './assets/home/vader3.jpg' className = {classes.carouselImage}/>
                </div>
                <div>
                    <img src = './assets/home/vader4.jpg' className = {classes.carouselImage}/>
                </div>
                <div>
                    <img src = './assets/home/riding.jpg' className = {classes.carouselImage}/>
                </div>
            </Carousel>
            <hr />

            <Card className = {classes.mainContainer}>
                <div className = {classes.content}>
                    <img 
                        src = './assets/home/news.jpg' 
                        alt = 'breaking news' 
                        style = {{width: '100%', height: '100%'}}/>
                </div>
                <div className = {classes.content}>
                    <h1 style = {{fontFamily: 'Starjedi'}}>our platform</h1>
                    <p className = {classes.paragraph}>
                        <ul style = {{textAlign: 'left', color: 'gray'}}>
                            <li>We're going to rebuild the death star and make the rebels pay for it!</li>
                            <li>We will open a criminal investigation into Palpatine and the attack on the death star. 
                                Was it an inside job? We must know the truth.</li>
                            <li>We will crush the rebellion and get back to focusing on what really matters!</li>
                            <li>And much more!</li>
                        </ul>
                    </p>
                    <p className = {classes.paragraph} style = {{marginLeft: '2vw', marginRight: '2vw'}}>
                        If you agree with any of this and want to see more, click below to see where I stand on the issues.
                    </p>
                    <Link to = '/issues'>
                    <Button 
                        className = {classes.button} 
                        variant = 'contained' 
                        color = 'primary'>
                        <p className = {classes.buttonText}><b>Issues</b></p>
                    </Button>
                </Link>
                </div>
            </Card>
            <Container className = {classes.textContainer}>
                <h1 style = {{fontFamily: 'Starjedi'}}>why vader?</h1>
                <p className = {classes.paragraph}>
                    Welcome to the official 2020 election campaign of Sith lord, master of the dark side, 
                    and Supreme Commander of the Imperial Fleet, Darth Vader. I am seeking the Sith nomination 
                    for emperor because I know what really bothers the people of the galaxy:
                </p>

                <VideoPanel 
                    name = "1. Sand - it's rough, coarse, and irritating, and it gets everywhere"
                    video = 'https://www.youtube.com/watch?v=2tLf1JO5bvE' />
                <VideoPanel
                    name = "2. When they can't find the droids they're looking for"
                    video = 'https://www.youtube.com/watch?v=532j-186xEQ'/>
                <VideoPanel
                    name = "3. When they are on the council, but they are not granted the rank of master"
                    video = 'https://www.youtube.com/watch?v=3tav3bI4M6I'/>
                <VideoPanel
                    name = "4. When the rebels escape with the death star plans"
                    video = 'https://www.youtube.com/watch?v=2Cev02IuIm0'/>
        
                <hr />
                <h2 className = {classes.heading}>
                    "Why do we need a new emperor? I like Palpatine."
                </h2>
                <blockquote style = {{color: 'gray'}}>
                    <b>"Don't let him kill me. I can't hold it any longer. I can't. I am too weak.
                        Anakin, help me! Help me! [...] I am too weak. Don't kill me, please."</b>
                </blockquote>
                <p style = {{color: 'gray', textAlign: 'center'}}>
                    -- <a style= {{color: '#ABB8C3'}} href = 'https://youtu.be/q0r4jNhG9Z4?t=160' target = '__blank'>
                        Emperor Palpatine
                    </a>
                </p>
                <img src = './assets/home/help-me.gif' />
                <p className = {classes.paragraph}>
                    Palpatine is old, pathetic, and weak. If I had not rescued him, he would have been defeated by the
                    Jedi master Mace Windu. He has ignored the sand problem for far too long and has yet to comment 
                    on his plans for when people can't find the droids they're looking for. He has failed to crush 
                    the rebellion and has allowed the empire's prized weapon, the death star, to be destroyed by rebel scum. 
                    Thousands of hard working men and women were killed in a cowardly attack while he did nothing. 
                    When I am in charge, we will never allow another tragedy like the death star. Remember, you can destroy
                    the emperor. It is your destiny.
                </p>
                <hr />
                <h2 className = {classes.heading}>
                    "What about a Jedi?"
                </h2>
                <blockquote style = {{color: 'gray'}}>
                    <b>"If you only knew the power of the dark side..."</b>
                </blockquote>
                <p style = {{color: 'gray', textAlign: 'center'}}>
                    -- <a style= {{color: '#ABB8C3'}} href = 'https://www.youtube.com/watch?v=iEzTO-T9H9o' target = '__blank'>
                        Darth Vader
                    </a>
                </p>
                <img src = './assets/home/vader-quote.gif'/>
                <p className = {classes.paragraph}>
                    <a style= {{color: '#ABB8C3'}} href = 'https://www.youtube.com/watch?v=SXrsjvmJSc8' target = '__blank'>
                        I see through the lies of the Jedi.</a> They claim to love democracy, but they
                        tried to overthrow a democratically elected chancellor because they feared the
                        dark side. <a 
                        style= {{color: '#ABB8C3'}} 
                        href = 'https://www.liveabout.com/star-wars-glossary-order-66-2958016' target = '__blank'>
                        After I destroyed them</a>,  they were forced to go into hiding for decades.
                    Now they're back and they've destroyed the death star, which was built with your tax dollars! 
                    With our combined strength, we can end this destructive conflict and bring order to the galaxy.  
                </p>
                <hr />
                <Card style = {{width: '100%', height: '100%', backgroundColor: '#111111'}}>
                    <img src = './assets/home/airport.jpg' style = {{width: '100%'}}/>
                </Card>
                <blockquote style = {{color: 'gray'}}>
                    <b>"I have brought peace to the Republic. I am more powerful than the Chancellor, I can overthrow him. 
                    And together you and I can rule the galaxy. Make things the way we want them to be."</b>
                </blockquote>
                <p style = {{color: 'gray', textAlign: 'right'}}>
                    -- <del>Anakin Skywalker</del> Darth Vader
                </p>
                <p style = {{color: 'gray', textAlign: 'left'}}> 
                    To learn more about me and why I am running for emperor, click below:
                </p>
                <Link to = '/about'>
                    <Button 
                        className = {classes.button} 
                        variant = 'contained' 
                        color = 'primary'>
                        <p className = {classes.buttonText}><b>Who is Darth Vader?</b></p>
                    </Button>
                </Link>
            </Container>
            <hr />

            <Card className = {classes.secondaryContainer} style = {{marginTop: '4vh'}}>
                <Container className = {classes.contentLarge}>
                    <h2 style = {{fontFamily: 'Starjedi'}}>show your support</h2>
                    <p className = {classes.paragraph}>
                        Do you support my campaign, but aren't sure how to show it? Is it your lifelong dream to have a
                        poster with my face on it? Head over to the official Vader 2020 campaign shop to see our collection
                        of Vader 2020 merchandise. It has everything from stickers to mugs to sweaters – 
                        all of your Darth Vader needs in one place! Just click below to see all the items you can choose from.
                    </p>
                    <Link to = '/shop'>
                        <Button 
                            className = {classes.button}
                            style = {{marginTop: '2vh'}} 
                            variant = 'contained' 
                            color = 'primary'>
                            <p className = {classes.buttonText}><b>Shop</b></p>
                        </Button>
                    </Link>
                </Container>
                <img src = './assets/home/vader-podium2.jpg' alt = 'flag patch' className = {classes.contentSmall}/>
            </Card>

            <Card className = {classes.secondaryContainer} style = {{float: 'right'}}>
                <img src = './assets/home/pointing.jpg' alt = 'vader pointing' className = {classes.contentSmall}/>
                <Container className = {classes.contentLarge}>
                    <h2 style = {{fontFamily: 'Starjedi'}}>the empire needs you</h2>
                    <p className = {classes.paragraph}>
                        I may be a master of evil and the dark side of the force, but I can't defeat Palpatine alone.
                        You can help by making a donation to my campaign. Together we can make the galaxy great again!
                    </p>
                    {loggedIn ?
                        <Link to = '/donate'>
                        <Button 
                            className = {classes.button}
                            style = {{marginTop: '2vh'}} 
                            variant = 'contained' 
                            color = 'primary'>
                            <p className = {classes.buttonText}><b>Donate</b></p>
                        </Button>
                    </Link>
                    :   
                    <div>
                        <p className = {classes.paragraph}>
                            You must be signed in to make a donation, so if you haven't already, click below
                            and sign up to let me know you're in.
                        </p>
                        <Link to = '/login'>
                            <Button 
                                className = {classes.button}
                                style = {{marginTop: '-2vh'}} 
                                variant = 'contained' 
                                color = 'primary'>
                                <p className = {classes.buttonText}><b>Sign Up</b></p>
                            </Button>
                        </Link>
                    </div>
                    }
                </Container>
            </Card>
        </div>
    );
  }