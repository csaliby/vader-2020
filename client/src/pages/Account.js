import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Error from '../components/Error';
import Axios from 'axios';

const useStyles = makeStyles(theme => ({
    title: {
        fontSize: '300%',
        float: 'left',
        fontFamily: 'Starjedi',
        color: 'gray',
        display: 'flex'
    },
    mainContainer: {
        paddingLeft: '2vw',
        backgroundColor: '#111111',
        opacity: '0.9',
        width: '40rem',
        minHeight: '94vh',
        color: 'white',
        display: 'flex',
        flexDirection: 'column',
    },
    background: {
        backgroundImage: "url('./assets/account/star-destroyer.jpg')",
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        position: 'absolute',
        width: '100%',
        left: '0',
        display: 'flex',
        flexDirection: 'column',
    },
    name: {
        //fontFamily: 'Starjedi',
        color: 'white',
        fontSize: '200%',
        textAlign: 'left'
    },
    item: {
        color: 'white',
        textAlign: 'left',
    },
    mainText: {
        marginLeft: '3vw',
        display: 'flex',
        textAlign: 'left',
        fontSize: '125%'
    },
    button: {
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        marginBottom: theme.spacing(2)
    },
    buttonText: {
        fontFamily: 'Alatsi',
        fontSize: '125%',
        margin: '0'
    },
}));

export default function Account({history, theme, loggedIn, currentUser, logout}){
    const classes = useStyles();

    const { email, name, phoneNumber, date, donations, totalDonated } = currentUser;
    const hasNumber = (phoneNumber !== '');

    const [ closeButtonClicked, setCloseButtonClicked ] = React.useState(false);

    const handleCloseClick = () => {
        setCloseButtonClicked(true);
    };

    const handleNoClick = () => {
        setCloseButtonClicked(false);
    };

    const handleYesClick = async () => {
        //const user = currentUser;
        try{
            Axios.delete(`/api/users/${currentUser._id}`, currentUser).then(response => {
                logout(undefined);
                history.push('/bye');
            });
        }catch(error){
            console.log(error);
        }
    };

    return (
        <div>
            {loggedIn ?
                <div className = {classes.background}>
                    <div style = {{display: 'flex', flexDirection: 'column'}}>
                        <Paper className = {classes.mainContainer}>
                            <h1 className = {classes.title}>account details</h1>
                            <h1 className = {classes.name}>Name:  {name}</h1>
                            <p className = {classes.item}>Email:  <div>{email}</div></p>
                            <p className = {classes.item}>Member since:  <div>{date}</div></p>
                            <p className = {classes.item}>Phone number:  
                                {hasNumber ?
                                    <div>{phoneNumber}</div>
                                :
                                    <div>none</div>
                                }
                            </p>
                            <p className = {classes.item}>Total amount donated:  ${totalDonated}</p>
                            {closeButtonClicked ?
                                <div>
                                    <p className = {classes.mainText}>
                                        Are you sure you want to close your account?
                                    </p>
                                    <div style = {{display: 'flex', flexDirection: 'row'}}>
                                        <Button 
                                            className = {classes.button}
                                            variant = 'contained' 
                                            color = 'secondary' 
                                            onClick = {handleNoClick}>
                                                <p className = {classes.buttonText}><b>No</b></p>
                                            </Button>
                                        <Button 
                                            className = {classes.button}
                                            variant = 'contained' 
                                            color = 'primary' 
                                            onClick = {handleYesClick}>
                                                <p className = {classes.buttonText}><b>Yes</b></p>
                                            </Button>
                                    </div>
                                </div>
                            :
                                <div>
                                    <p className = {classes.mainText}>
                                        Click the button below to close your account. This action cannot be undone.
                                    </p>
                                    <Button variant = 'contained' color = 'primary' onClick = {handleCloseClick}>
                                        <p className = {classes.buttonText}><b>Close Account</b></p>
                                    </Button>
                                </div>
                            }

                        </Paper>
                    </div>
                </div>
            :
                <Error message = 'You must be signed in to access this page.'/>
            }
        </div>
    );
}
