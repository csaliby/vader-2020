import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Container from '@material-ui/core/Container'
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import ReactPlayer from 'react-player'

const useStyles = makeStyles(theme => ({
    textContainer: {
        width: '30%',
        height: '100%',
        justifyContent: 'center',
        marginLeft: '2vw',
        marginRight: '2vw'
    },
    mainContainer: {
        backgroundColor: '#111111',
        width: '100%',
        height: '100%',
        paddingTop: '2vh'
    },
    card: {
        width: '100%',
        height: '30rem',
        //marginBottom: '2rem',
        backgroundColor: '#111111',
        display: 'flex',
        flexDirection: 'row',
        color: 'white'
    },
    mainImg: {
        width: '70%',
        height: '100%',
        display: 'flex',
    },
    expansionPanel: {
        backgroundColor: '#222222',
        color: 'white',
        width: '100%',
    },
    panelCard: {
        width: '100%',
        height: '100%',
        //maxHeight: '40rem',
        backgroundColor: '#111111',
        color: 'gray',
        display: 'flex',
        flexDirection: 'row',
    },
    panelImg: {
        width: '70%',
        height: '100%'
    },
    paragraph: {
        lineHeight: '1.5rem',
        textAlign: 'left',
    },
    heading: {
        fontFamily: 'Starjedi',
        color: 'gray'
    }
}));
//<img src = './assets/vader-img.webp' className = {classes.mainImg}/>
export default function Issues({history, theme, resetPosition}) {
    resetPosition();
    const classes = useStyles();

    return (
        <div style = {{display: 'flex', flexDirection: 'column', justifyContent: 'center', color: 'white'}}>
            <Container style = {{color: 'white'}}>
                <h1 style = {{fontFamily: 'Starjhol', fontSize: '400%', marginBottom: '2vh'}}>issues</h1>
                <h3 style = {{fontFamily: 'Starjedi', marginTop: '-2vh'}}>vader on the issues</h3>
                <hr />
            </Container>
            <Card className = {classes.card} style = {{marginTop: '2rem'}}>
                <div className = {classes.textContainer}>
                    <h1 className = {classes.heading}>a new deal</h1>
                    <p className = {classes.paragraph}>
                        <blockquote style = {{color: 'gray', fontFamily: 'Starjedi', textAlign: 'center'}}>
                            "i am altering the deal, pray i don't alter it any further"
                        </blockquote>
                        <p style = {{textAlign: 'right', color: 'gray'}}>
                            -- Darth Vader
                        </p>
                        To the rebels who seek to destabilize the empire, I have this to say -
                        you underestimate the power of the dark side. I am offering a new deal for the galaxy. 
                        The day-to-day issues facing the people of our great empire are of immense importance to me. 
                        Below are some of the policies I will enact if elected that will make the galaxy a more 
                        civilized place.
                    </p>
                </div>
                <Carousel 
                    className = {classes.mainImg} 
                    showArrows = {true} 
                    showThumbs = {false} 
                    showStatus = {false}
                    infiniteLoop = {true}
                    autoPlay = {true}>
                    <div>
                        <img src="./assets/issues/issues-img1.jpg" />
                    </div>
                    <div>
                        <img src = './assets/issues/issues-img2.jpg' />
                    </div>
                    <div>
                        <img src = './assets/issues/issues-img3.jpg' />
                    </div>
                </Carousel>
            </Card>
            <Container fixed className = {classes.mainContainer}>
                <hr />
                <ExpansionPanel  className = {classes.expansionPanel}>
                    <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon style = {{color: 'white'}}/>}
                        id="issue1-header">
                        Jedi Statues
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Card className = {classes.panelCard}>
                            <div className = {classes.textContainer}>
                                <h1 className = {classes.heading}>tear them down</h1>
                                <p className = {classes.paragraph}>
                                    Statues such as the one shown on the right, which depict figures of the old, fanatical,
                                    Jedi Order are still standing all across the empire. This one in particular
                                    depicts the old Jedi master Yoda, who is still in hiding somewhere. He is training 
                                    new Jedi who will one day attempt to destroy our empire. It is unimaginable that Palpatine 
                                    would tolerate such unpatriotic displays with the rebellion ongoing and being led by the
                                    Jedi-in-training Luke Skywalker. When I am emperor we will tear these offensive symbols down!
                                </p>
                            </div>
                            <img src = './assets/issues/yoda-statue.jpg' className = {classes.panelImg} />
                        </Card>
                    </ExpansionPanelDetails>
                </ExpansionPanel>

                <ExpansionPanel  className = {classes.expansionPanel}>
                    <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon style = {{color: 'white'}}/>}
                        id="issue2-header">
                        Healthcare
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Card className = {classes.panelCard}>
                            <div className = {classes.textContainer}>
                                <h1 className = {classes.heading}>medical droids for all</h1>
                                <p className = {classes.paragraph}>
                                    It is my firm belief that all patriotic citizens of the empire are entitled to basic
                                    medical care. To that end, I will provide every person<sup>*</sup> in the empire 
                                    with a state-of-the-art medical droid to see to their medical needs. It is unfathomable
                                    that members of the imperial senate have guaranteed medical care, but our loyal
                                    citizens do not.
                                </p>
                                <p className = {classes.paragraph} style = {{fontSize: '75%'}}>
                                    <sup>*</sup>excluding Jedi and rebel scum
                                </p>
                            </div>
                            <img src = './assets/issues/medical-droid.jpeg' className = {classes.panelImg} />
                        </Card>
                    </ExpansionPanelDetails>
                </ExpansionPanel>

                <ExpansionPanel  className = {classes.expansionPanel}>
                    <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon style = {{color: 'white'}}/>}
                        id="issue2-header">
                        Education
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Card className = {classes.panelCard}>
                            <div className = {classes.textContainer}>
                                <h1 className = {classes.heading}>teach the tragedy</h1>
                                <p className = {classes.paragraph}>
                                    Have you ever heard the tragedy of Darth Plagueis the Wise? I thought not. It's not
                                    a story the Jedi would tell you. For too long, the Jedi have banned this great piece
                                    of history from our schools. When I am emperor, we will teach the tragedy in schools
                                    again.
                                </p>
                            </div>
                            <ReactPlayer controls url = 'https://www.youtube.com/watch?v=05dT34hGRdg'/>
                        </Card>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                
                <ExpansionPanel className = {classes.expansionPanel} style = {{marginBottom: '2vh'}}>
                    <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon style = {{color: 'white'}}/>}
                        id="issue3-header">
                        Disintegrations
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Card className = {classes.panelCard}>
                            <div className = {classes.textContainer}>
                                <h1 className = {classes.heading}>no disintegrations</h1>
                                <p className = {classes.paragraph}>
                                    The bounty hunting industry has gotten out of hand in recent years, and there are
                                    no imperial regulations in place against bounty hunter disintegrations. How are people
                                    supposed to know if their hitmen actually got the right target?
                                    As you may know, I have been an outspoken critic of disintegrations for many years.
                                    When I am emperor, we will pass strict anti-disintegration laws
                                </p>
                            </div>
                            <ReactPlayer controls url = 'https://www.youtube.com/watch?v=-064XumUDbg'/>
                        </Card>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </Container>


            <Card className = {classes.card} style = {{marginTop: '2rem'}}>
                <Carousel 
                    className = {classes.mainImg} 
                    showArrows = {true} 
                    showThumbs = {false} 
                    showStatus = {false}
                    infiniteLoop = {true}
                    autoPlay = {true}>
                    <div>
                        <img src="./assets/issues/issues-img4.jpg" />
                    </div>
                    <div>
                        <img src = './assets/issues/issues-img5.jpg' />
                    </div>
                </Carousel>
                <div className = {classes.textContainer}>
                    <h1 className = {classes.heading}>security</h1>
                    <p className = {classes.paragraph}>
                        <blockquote style = {{color: 'gray', fontFamily: 'Starjedi', textAlign: 'center'}}>
                            "you are unwise to lower your defenses"
                        </blockquote>
                        <p style = {{textAlign: 'right', color: 'gray'}}>
                            -- Darth Vader
                        </p>
                        The empire used to be strong. We hunted down the Jedi and got rid of them. Now look at us. 
                        The Jedi are coming back, the death star is gone, and our fleet is using TIE fighters from decades ago. 
                        We can't even stop the rebels anymore. When I'm emperor, we're going to increase the imperial 
                        defense budget, rebuild our fleet, and <a 
                            href = 'https://www.youtube.com/watch?v=WIRMkV0Kvxw' 
                            target = '__blank'
                            style= {{color: 'gray'}}>
                            bring peace, freedom, justice, and security to my new empire.</a>
                    </p>
                </div>
            </Card>
            <Container fixed className = {classes.mainContainer} style = {{marginBottom: '2rem'}}>
                <hr />
                <ExpansionPanel className = {classes.expansionPanel}>
                    <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon style = {{color: 'white'}}/>}
                        id="issue4-header">
                        The Death Star
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Card className = {classes.panelCard}>
                            <div className = {classes.textContainer}>
                                <h1 className = {classes.heading}>a new death star</h1>
                                <p className = {classes.paragraph}>
                                    When the death star was destroyed by the rebels, the empire lost its most
                                    powerful weapon. How stupid are we that we let this happen? First, it was built with
                                    a giant security flaw, then we let the plans leak! The death star was vital to our 
                                    internal and external defense, and must be rebuilt. As emperor, I will immediately 
                                    begin building a great, big, beautiful new death star. And this time, we won't let
                                    it be destroyed.
                                </p>
                            </div>
                            <img src = './assets/issues/deathstar2.jpg' className = {classes.panelImg} />
                        </Card>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                
                <ExpansionPanel className = {classes.expansionPanel} style = {{marginBottom: '2vh'}}>
                    <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon style = {{color: 'white'}}/>}
                        id="issue5-header">
                        The Rebellion
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Card className = {classes.panelCard}>
                            <div className = {classes.textContainer}>
                                <h1 className = {classes.heading}>an end to the rebellion</h1>
                                <p className = {classes.paragraph}>
                                    The rebels are traitors to the empire and must be crushed. When they destroyed
                                    the death star, they killed thousands of loyal soldiers of the empire as well
                                    as innocent workers. The day that I become emperor will be a glorious day. It will
                                    see the end of the rebellion, the Jedi, and the conflicts ravaging the empire.
                                </p>
                            </div>
                            <ReactPlayer controls url = 'https://www.youtube.com/watch?v=XkBPP5c63q0' />
                        </Card>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </Container>
        </div>
    )
}