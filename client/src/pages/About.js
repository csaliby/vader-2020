import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import 'react-responsive-carousel/lib/styles/carousel.min.css';

const useStyles = makeStyles(theme => ({
    background: {
        backgroundImage: "url('./assets/about/death-star-firing.png')",
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        position: 'absolute',
        width: '100%',
        left: '0',
        display: 'flex',
        flexDirection: 'column',
    },
    mainContainer: {
        paddingLeft: '2vw',
        backgroundColor: '#111111',
        opacity: '0.9',
        width: '40rem',
        minHeight: '94vh',
        color: 'white',
        display: 'flex',
        flexDirection: 'column',
    },
    title: {
        fontSize: '300%',
        float: 'left',
        fontFamily: 'Starjedi',
        color: 'gray',
        display: 'flex'
    },
    body: {
        marginLeft: '3vw',
        marginRight: '2rem',
        display: 'flex',
        textAlign: 'left',
        lineHeight: '1.5rem'
    }

}));

export default function About({history, theme}) {
    const classes = useStyles();

    return (
        <div className = {classes.background}>
            <div style = {{display: 'flex', flexDirection: 'column'}}>
                <Paper className = {classes.mainContainer}>
                    <h1 className = {classes.title}>who is darth vader?</h1>
                    <p className = {classes.body}>Darth Vader was born as Anakin Skywalker to a single mother, a slave, on the desert planet 
                    Tatooine. He was discovered by Qui-Gon Jinn and Obi-Wan Kenobi (his former Jedi mentor), and was thought to be "born of the force,"
                    meaning that he had no biological father. The Jedi believed him to be the Chosen One, and took him from Tatooine to be 
                    raised as a Jedi. He underwent extensive indoctrination by his mentors and Jedi leaders. When the Jedi
                    attempted to overthrow Chancellor Palpatine, Vader turned away from the Jedi Order and embraced the
                    dark side of the force, becoming a Sith.</p>
                    <p className = {classes.body}>After joining the Sith, he served the empire loyally 
                    for years as Palpatine's second in command. He played a major role in the destruction of the treasonous Jedi in the years following the
                    foundation of the empire. However, Vader believes Palpatine has grown weak since the start of the rebellion 
                    and feels that he should resign as emperor. Vader feels that he has been very effective at combatting
                    the Jedi and the rebellion over the years, and would like to continue to do so as your emperor. </p>
                    <p className = {classes.body} style = {{marginBottom: '2rem'}}>His dedication has been
                    fierce, and he has even gone as far as to fight against his own son, Luke Skywalker, who has taken on a leadership
                    role in the rebellion. He currently has two children: Leia Organa and Luke Skywalker.</p>
                </Paper>
            </div>
        </div>
    );
}