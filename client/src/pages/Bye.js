import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    heading: {
        fontFamily: 'Starjhol',
        fontSize: '200%',
        color: 'white',
        height: '100%'
    },
    message: {
        color: 'white'
    },
    button: {
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        marginBottom: theme.spacing(2)
    },
    buttonText: {
        fontFamily: 'Alatsi',
        fontSize: '125%',
        margin: '0'
    }
}));

export default function Bye({history, theme}){
    const classes = useStyles();

    return (
        <div style = {{backgroundColor: 'black'}}>
            <h1 className = {classes.heading}>
                if you will not fight, then you will meet your destiny
            </h1>
            <img src = './assets/error/no.gif' style = {{height: '30%', width: '45%'}}/>
            <h3 className = {classes.message}>
                Your account has been closed. We are sorry to see you go! Click the button below to return home:
            </h3>
            <Link to = '/'>
                <Button 
                    color = 'primary' 
                    variant = 'contained'
                    className = {classes.button}>
                        <p className = {classes.buttonText}><b>Return</b></p>
                </Button>
            </Link>
        </div>
    );
}