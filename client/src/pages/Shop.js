import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper'
import Axios from 'axios';

const useStyles = makeStyles(theme => ({
    text: {
        color: 'gray',
        marginLeft: '1vw',
        marginRight: '1vw',
        paddingTop: '2vh'
    },
    mainContainer: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        width: '100%',
        height: '100%',
        marginTop: '1.5vh',
        paddingTop: '5vh',
        backgroundColor: '#111111',
        opacity: '0.8',
        color: 'white'
    },
    row: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        marginBottom: '4vh'
    },
    card: {
        display: 'flex',
        flexDirection: 'column',
        width: '20%',
        height: '30rem',
        marginBottom: '5vh',
        marginLeft: '2vw',
        marginRight: '2vw',
        backgroundColor: '#222222'
    },
    media: {
        height: '50%',
        width: '100%'
    },
    content: {
        height: '40%',
        width: '100%',
        verticalAlign: 'bottom',
    },
    item: {
        fontFamily: 'Starjedi',
        color: 'white',
        marginLeft: '1vw',
        marginRight: '1vw',
        marginBottom: '-2vh'
    }
}));

export default function Shop({history, theme, resetPosition}) {
    resetPosition();
    const classes = useStyles();

    return (
        <div style = {{ justifyContent: 'center', color: 'white'}}>
            <h1 style = {{fontFamily: 'Starjhol', fontSize: '400%'}}>shop</h1>
            <hr />
            <Paper className = {classes.mainContainer}>
                <Container className = {classes.row}>
                    <Card className = {classes.card}>
                        <img src = '/assets/shop/mug.jpg' className = {classes.media} />
                        <h3 className = {classes.item}>vader 2020 mug</h3>
                        <p className = {classes.text}>A decorative mug commemorating Vader's historic 2020 election campaign</p>
                        <p className = {classes.text}>$6.99</p>
                    </Card>
                    <Card className = {classes.card}>
                        <img src = '/assets/shop/sticker2.jpg' className = {classes.media} />
                        <h3 className = {classes.item}>vader 2020 sticker</h3>
                        <p className = {classes.text}>A sticker for showing off your eternal loyalty to Lord Vader</p>
                        <p className = {classes.text}>$1.99</p>
                    </Card>
                    <Card className = {classes.card}>
                        <img src = '/assets/shop/sweater.png' className = {classes.media} />
                        <h3 className = {classes.item}>vader 2020 sweater</h3>
                        <p className = {classes.text}>Keep warm on Hoth with this official Vader 2020 sweater</p>
                        <p className = {classes.text}>$19.99</p>
                    </Card>
                    <Card className = {classes.card}>
                        <img src = '/assets/shop/poster.jpg' className = {classes.media} />
                        <h3 className = {classes.item}>vader 2020 poster</h3>
                        <p className = {classes.text}>A poster showcasing the dark lord himself in all his glory</p>
                        <p className = {classes.text}>$10.99</p>
                    </Card>
                </Container>
                <Container className = {classes.row}>
                    <Card className = {classes.card}>
                        <img src = '/assets/shop/laptop-skin.jpg' className = {classes.media} />
                        <h3 className = {classes.item}>vader 2020 laptop skin</h3>
                        <p className = {classes.text}>A laptop skin showing off your support for Lord Vader</p>
                        <p className = {classes.text}>$8.99</p>
                    </Card>
                    <Card className = {classes.card}>
                        <img src = '/assets/shop/yard-sign.jpg' className = {classes.media} />
                        <h3 className = {classes.item}>yard sign</h3>
                        <p className = {classes.text}>Let your neighbors tremble at the strength of your support</p>
                        <p className = {classes.text}>$15.99</p>
                    </Card>
                    <Card className = {classes.card}>
                        <img src = '/assets/shop/troops-sticker.jpg' className = {classes.media} />
                        <h3 className = {classes.item}>stormtrooper sticker</h3>
                        <p className = {classes.text}>A decorative sticker showing support for our brave stormtroopers</p>
                        <p className = {classes.text}>$3.99</p>
                    </Card>
                    <Card className = {classes.card}>
                        <img src = '/assets/shop/flag-patch.jpg' className = {classes.media} />
                        <h3 className = {classes.item}>vader 2020 flag patch</h3>
                        <p className = {classes.text}>A patriotic patch bearing Lord Vader's likeness</p>
                        <p className = {classes.text}>$7.99</p>
                    </Card>
                </Container>
            </Paper>
        </div>
    );
}