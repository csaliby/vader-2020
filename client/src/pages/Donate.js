import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import TextField from'@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Axios from 'axios';
import Error from '../components/Error';

const useStyles = makeStyles(theme => ({
    background: {
        backgroundImage: "url('./assets/donate/donate-background.jpg')",
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        position: 'absolute',
        width: '100%',
        left: '0',
        display: 'flex',
        flexDirection: 'column',
    },
    mainContainer: {
        paddingLeft: '2vw',
        backgroundColor: '#111111',
        opacity: '0.9',
        width: '40rem',
        minHeight: '94vh',
        color: 'white',
        display: 'flex',
        flexDirection: 'column',
    },
    title: {
        fontSize: '300%',
        float: 'left',
        fontFamily: 'Starjedi',
        color: 'gray',
        display: 'flex'
    },
    body: {
        marginLeft: '3vw',
        display: 'flex',
        textAlign: 'left',
        fontSize: '125%'
    },
    textField: {
        margin: theme.spacing(2),
        display: 'flex'
    },
    text: {
        color: 'white'
    },
    placeholder: {
        color: '#B3B3B3'
    },
    buttonText: {
        fontFamily: 'Alatsi',
        fontSize: '125%',
        margin: '0'
    },
    button: {
        width: '40%',
        padding: '0'
    }
}));

export default function Donate({history, theme, loggedIn, currentUser, setTier}) {
    const classes = useStyles();

    const [ amount, setAmount ] = React.useState(0);

    const handleAmountChange = event => {
        setAmount(event.target.value);
    };

    const isValidDonation = () => { return (amount > 0 && amount < 1001); }

    //Donation tier is used to determine the thank you message the user receives
    const isTier0 = () => { return (amount > 0 && amount < 250); }

    const isTier1 = () => { return (amount > 249 && amount < 500); }

    const isTier2 = () => { return (amount > 499 && amount < 1001); }

    const handleDonationClick = async () => {
        const donation = amount;
        //console.log(donation);
        //console.log(currentUser);
        if(isValidDonation()){
            if(isTier0()){ setTier(0); }
            else if(isTier1()) { setTier(1); }
            else if(isTier2()) { setTier(2); }

            try{
                currentUser.donations.push(+donation);
                const newTotal = +currentUser.totalDonated + +donation;
                currentUser.totalDonated = newTotal;
                await Axios.put(`/api/users/donate/${currentUser._id}`, currentUser).then(response => {
                    history.push('/thanks');
                });
            }catch(error) {
                console.log(error.message);
            }
        }
        else{
            alert('This is not a valid donation amount. Please enter a number between 0 and 1000');
        }
    };

    return (
        <div>
            {loggedIn ?
                <div className = {classes.background}>
                    <div style = {{display: 'flex', flexDirection: 'column'}}>
                        <Paper className = {classes.mainContainer}>
                            <h1 className = {classes.title}>Make a donation</h1>
                            <p className = {classes.body}>
                                Make a donation to send a message to the rebel traitors. They will join us or die.
                            </p>
                            <TextField
                                className = {classes.textField}
                                id = 'amount-input'
                                label = '$ Amount'
                                InputProps = {{ className: classes.text}}
                                InputLabelProps = {{ className: classes.placeholder }}
                                margin = 'normal'
                                variant = 'outlined'
                                type = 'number'
                                onChange = {handleAmountChange}
                                required
                            />
                            <Button
                                onClick = {handleDonationClick}
                                variant = 'contained'
                                color = 'primary'
                                className= {classes.button}>
                                    <p className = {classes.buttonText}><b>Donate</b></p>
                            </Button>
                        </Paper>
                    </div>
                </div>
            :
                <Error message = 'You must be signed in to access this page.'/>
            }
        </div>
    );
}