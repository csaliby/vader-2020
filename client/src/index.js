import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import './fonts/Starjedi.ttf'
import Carousel from 'react-bootstrap/Carousel'

ReactDOM.render(<App />, document.getElementById('root'));
serviceWorker.unregister();
