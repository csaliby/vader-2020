import React, { Component } from 'react';
import NavBar from './components/Navbar';
import Container from '@material-ui/core/Container';
import { ThemeProvider } from '@material-ui/styles';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { createMuiTheme, makeStyles } from '@material-ui/core/styles';
import Home from "./pages/Home"
import Login from "./pages/Login";
import Issues from "./pages/Issues";
import About from "./pages/About";
import Shop from "./pages/Shop";
import Donate from "./pages/Donate";
import ThankYou from "./pages/ThankYou";
import Account from "./pages/Account";
import Bye from "./pages/Bye";
import './App.css';

class App extends Component{
  
  state = {
      currentUser: {},
      loggedIn: false,
      donationTier: 0
    };

    changeUser = user => {
      if(user){
        this.setState({ ...this.state, loggedIn: true });
        this.setState({ ...this.state, currentUser: user });
      }
      else{
        this.setState({ ...this.state, loggedIn: false });
        this.state.currentUser = {};
      }
      console.log(this.state.currentUser);
    };

    changeTier = tier => {
      this.setState({ ...this.state, donationTier: tier });
    }

    resetPosition = () => { window.scrollTo(0,0); }

    render(){
      
      const theme = createMuiTheme({
        typography: {
          button: {
            textTransform: 'none',
            textDecoration: 'none'
          }
        }
      });

      const currentUser = this.state.currentUser;
      const loggedIn = this.state.loggedIn;
      const donationTier = this.state.donationTier;

      
      return (
        <div className = "App" style = {{ height: '100vh', display: 'flex', flexDirection: 'column' }}>
          <Router>
            <NavBar 
              loggedIn = {loggedIn} 
              logOut = {this.changeUser}/>
            <ThemeProvider theme = {theme}>
              <Container fixed>
                <Route exact = {true} path = '/' render = {({history}) => 
                  <Home
                    login = {this.changeUser}
                    history = {history}
                    theme = {theme}
                    loggedIn = {loggedIn}
                    resetPosition = {this.resetPosition}
                  />
                } />
                <Route exact = {true} path = '/login' render = {({history}) => 
                  <Login
                    login = {this.changeUser}
                    theme = {theme}
                    history = {history}
                  />
                } />
                <Route exact = {true} path = '/issues' render = {({history}) => 
                  <Issues
                    history = {history}
                    theme = {theme}
                    resetPosition = {this.resetPosition}
                  />
                } />
                <Route exact = {true} path = '/about' render = {({history}) => 
                  <About
                    history = {history}
                    theme = {theme}
                  />
                } />
                <Route exact = {true} path = '/shop' render = {({history}) =>
                  <Shop
                    history = {history}
                    theme = {theme}
                    resetPosition = {this.resetPosition}
                  />
                } />
                <Route exact = {true} path = '/donate' render = {({history}) => 
                  <Donate 
                    history = {history}
                    theme = {theme}
                    loggedIn = {loggedIn}
                    currentUser = {currentUser}
                    setTier = {this.changeTier}
                  />
              } />
              <Route exact = {true} path = '/thanks' render = {({history}) =>
                <ThankYou
                  history = {history}
                  theme = {theme}
                  tier = {donationTier}
                  loggedIn = {loggedIn}
                />
              } />
              <Route exact = {true} path = '/account' render = {({history}) => 
                <Account
                  history = {history}
                  theme = {theme}
                  loggedIn = {loggedIn}
                  currentUser = {currentUser}
                  logout = {this.changeUser}
                />
              } />
              <Route exact = {true} path = '/bye' render = {({history}) => 
                <Bye
                  history = {history}
                  theme = {theme}
                />
              } />
              </Container>
            </ThemeProvider>
          </Router>
        </div>
      );
    }
}

export default App;
