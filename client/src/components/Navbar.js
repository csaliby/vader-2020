import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

const useStyles = makeStyles(theme => ({
    mainTitle: {
        color: 'red',
        fontFamily: 'Starjedi',
        textAlign: 'left',
        textDecoration: 'none',
        marginLeft: theme.spacing(2),
        flexGrow: 1
    },
    option: {
        textDecoration: 'none',
        color: '#B71C1C',
        display: 'flex',
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        "&:hover": {
            color: 'red'
        }
    },
    menuItem: {
        backgroundColor: '#222222',
        color: '#B71C1C',
        textDecoration: 'none',
        width: '100%',
        height: '100%',
        "&:hover": {
            backgroundColor: '#333333',
            color: 'red'
        }
    },
    menu: {
        backgroundColor: '#222222'
    }
}));

export default function NavBar({history, loggedIn, logOut}) {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleLogOut = () => { 
        logOut(undefined);
        closeMenu();
    };

    const closeMenu = () => { setAnchorEl(null); }

    const openMenu = event => { setAnchorEl(event.currentTarget); }

    return (
        <AppBar position = 'sticky' style = {{backgroundColor: '#111111', marginBottom: '0rem', height: '4rem'}}>
            <Toolbar disableGutters>
                <Link to = '/' className = {classes.mainTitle}>
                    <Typography
                        style = {{fontFamily: 'Starjedi', color: 'red'}}
                        variant = 'h6'>
                        vader 2020
                    </Typography>
                </Link>
                <Link to = '/issues' className = {classes.option} style = {{marginLeft: '0rem'}}>
                    <Typography style = {{fontFamily: 'Alatsi'}} variant = 'h6'>Issues</Typography>
                </Link>
                <Link to = '/about' className = {classes.option}>
                    <Typography style = {{fontFamily: 'Alatsi'}} variant = 'h6'>About</Typography>
                </Link>
                <Link to = '/shop' className = {classes.option}>
                    <Typography style = {{fontFamily: 'Alatsi'}} variant = 'h6'>Shop</Typography>
                </Link>
                {loggedIn ?
                    <div style = {{display: 'block'}}>
                        <IconButton color = 'inherit' onClick = {openMenu}>
                            <AccountCircleIcon fontSize = 'large' className = {classes.option} />
                        </IconButton>
                        <Menu
                            classes = {{paper: classes.menu}}
                            id="simple-menu"
                            anchorEl={anchorEl}
                            keepMounted
                            open={Boolean(anchorEl)}
                            onClose={closeMenu}>
                            <Link to = '/account' className = {classes.menuItem}>
                                <MenuItem onClick = {closeMenu} className = {classes.menuItem}>
                                    My Account
                                </MenuItem>
                            </Link>
                            <Link to = '/donate' className = {classes.menuItem}>
                                <MenuItem className = {classes.menuItem}>
                                    Donate
                                </MenuItem>
                            </Link>
                            <MenuItem 
                                onClick={handleLogOut} 
                                className = {classes.menuItem}>
                                    Log Out
                            </MenuItem>
                        </Menu>
                    </div>

                :
                    <Link to = '/login' className = {classes.option}>
                        <Typography style = {{fontFamily: 'Alatsi'}} variant = 'h6'>Join Me</Typography>
                    </Link>
                }
            </Toolbar>
        </AppBar>
    )
}