import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import ReactPlayer from 'react-player';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles(theme => ({
    expansionPanel: {
        backgroundColor: '#222222',
        color: 'white',
        //width: '100%'
    }
}));

export default function VideoPanel({name, video}) {
    const classes = useStyles();

    return (
        <ExpansionPanel className = {classes.expansionPanel}>
            <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon style = {{color: 'white'}}/>}>
                {name}
            </ExpansionPanelSummary>
            <ExpansionPanelDetails style = {{justifyContent: 'center'}}>
                <ReactPlayer controls url = {video}/>
            </ExpansionPanelDetails>
        </ExpansionPanel>
    );
}