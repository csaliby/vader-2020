const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const cors = require('cors');

const database = process.env.MONGODB_URL;
const clientAppDirectory = path.join(__dirname, '../public', 'build');
const usersRouter = require('./routes/users');
const server = express();

server.use(express.json());
server.use(express.urlencoded({ extended: false }));
server.use(cors());
server.use('/api/users', usersRouter);
server.use(express.static(clientAppDirectory));
server.get('/*', (request, response) => {
    const indexPath = path.join(clientAppDirectory, 'index.html');
    return response.sendFile(indexPath);
});

//mongoose.connect('mongodb://localhost/vader2020', { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.connect(database, { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;
db.once('open', async () => {
   console.log('connected to mongodb');
});

server.listen(process.env.PORT || 4000);
