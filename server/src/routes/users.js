const express = require('express');
const router = express.Router();
const User = require('../models/user');
const bcrypt = require('bcrypt');

const getUserById = async (request, response, next) => {
    try {
        user = await User.findById(request.params.id);
        if (user === null) {
            return response.status(404).json({ message: 'User does not exist'})
        }
    } catch(error){
        return response.status(500).json({ message: error.message })
    }
  
    response.user = user;
    next();
};
//--------------------------------------------------------------
router.post('/', async (request, response) => {
    const { email, name, phoneNumber, password } = request.body;
    const passwordDigest = bcrypt.hashSync(password, 10);
    const user = new User({
        name,
        email,
        phoneNumber,
        password: passwordDigest,
        donations: [0],
        totalDonated: 0,
        date: new Date()
    });
  
    try {
        const existingUser = await User.findOne({ email });
        if (!existingUser) {
            const newUser = await user.save();
            response.status(201).json(newUser);
        } else {
            response.json({ message: 'An account with that email already exists' });
        }
    } catch (error) {
        response.status(500).json({ message: error.message });
    }
});
//--------------------------------------------------------------
router.post('/auth', async (request, response) => {
    const { email, password } = request.body;
    const user = await User.findOne({ email });

    if(user === null) {
        return response.json(undefined);
    }

    const correctPassword = bcrypt.compareSync(password, user.password);
    if(correctPassword) {
        return response.json(user);
    }
    else{
        return response.json(undefined);
    }
});
//--------------------------------------------------------------
//Add a donation to a user
router.put('/donate/:id', async (request, response) => {
    try{
        User.findByIdAndUpdate({ _id: request.params.id }, request.body).then(() => {
            User.findOne({ _id: request.params.id }).then(user => {
                response.send(user);
            });
        });
    }catch(error){
        response.status(500).json({ message: error.message });
    }
});
//--------------------------------------------------------------
router.delete('/:id', getUserById, async (request, response) => {
    try {
        await response.user.remove();
        response.json({ message: `User @${response.user.email} has been deleted.` });
    } catch(error) {
        response.status(500).json({ message: error.message });
    }
});

module.exports = router;
  