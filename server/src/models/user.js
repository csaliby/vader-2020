const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    name: String,
    email: String,
    phoneNumber: String,
    password: String,
    donations: Array,
    totalDonated: Number,
    date: Date
});

const User = mongoose.model('User', userSchema);

module.exports = User;